<?php

namespace Stocktape\Http\Middleware;

use Closure;
use Sentinel;
class BdcrepMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug=='Bdcrep'){

            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
