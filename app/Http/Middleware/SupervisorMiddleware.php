<?php

namespace Stocktape\Http\Middleware;

use Closure;
use Sentinel;
class SupervisorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug=='Supervisor'){
            
            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
