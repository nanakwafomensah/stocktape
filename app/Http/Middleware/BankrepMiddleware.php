<?php

namespace Stocktape\Http\Middleware;

use Closure;
use Sentinel;
class BankrepMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Sentinel::check()&& Sentinel::getUser()->roles()->first()->slug=='Bankrep'){

            return $next($request);
        }else{
            return redirect('/');
        }
    }
}
