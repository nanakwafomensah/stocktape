<?php

namespace Stocktape\Http\Controllers;

use Illuminate\Http\Request;
use Stocktape\Omc;
use DB;
class omcController extends Controller
{
    //
    public function index(){
        $omcs=DB::table('omcs')->orderBy('created_at', 'desc')->get();
        return view('admin.entry.omc')->with(['omcs'=>$omcs]);
    } 
    public function omcfieldrep(){
        $omcs=DB::table('omcs')->orderBy('created_at', 'desc')->get();
        return view('fieldrep.omcfieldrep')->with(['omcs'=>$omcs]);
    }

    public function save( Request $request){

        try{
            $m = new Omc();

            $m->name=$request->name;
            if($m->save()){
                $notification=array(
                    'message'=>"New Omc  has been Succesfully Added",
                    'alert-type'=>'success'
                );
                return redirect('omc')->with($notification);
            }
        }catch(Exception $e){
            $notification=array(
                'message'=>'A Error Occured',
                'alert-type'=>'error'
            );
            return redirect('omc')->with($notification);
        }
    }

    public function update(Request $request){

        try{
            $m = Omc::findorfail($request->idedit);
            $m->name=$request->nameedit;
            if($m->update()){

                $notification=array(
                    'message'=>"Details  has been Succesfully updated",
                    'alert-type'=>'success'
                );
                return redirect('omc')->with($notification);

            }
        }catch(Exception $e){
            $notification=array(
                'message'=>"Details  failed to updated",
                'alert-type'=>'success'
            );
            return redirect('omc')->with($notification);
        }
    }
    public function delete(Request $request){

        try{
            $m = Omc::findorfail($request->iddelete);
            if($m->delete()){
                $notification=array(
                    'message'=>"Bank has been deleted",
                    'alert-type'=>'success'
                );
                return redirect('omc')->with($notification);
            }


        }catch(Exception $e){
            $notification=array(
                'message'=>"An error occured",
                'alert-type'=>'error'
            );
            return redirect('omc')->with($notification);
        }
    }

}
