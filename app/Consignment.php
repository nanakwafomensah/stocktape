<?php

namespace Stocktape;

use Illuminate\Database\Eloquent\Model;

class Consignment extends Model
{
    //
    public function releases(){
       return $this->hasMany('Stocktape\Consignmentrelease');
    }

}

