<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Daily Stock taking report</title>
    <style>
        #datarecord,#brieftable {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
            font-size: 12px;
        }

        #datarecord td, #datarecord th {
            border: 1px solid black;
            padding: 8px;
        }

        #datarecord tr:nth-child(even){background-color: #f2f2f2;}

        #datarecord tr:hover {background-color: #ddd;}

        #datarecord th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #9CD8F0  ;
            color: black;
            font-size: 12px;
        }
        div.page
        {
            page-break-after: always;
            page-break-inside: avoid;
        }
        #brieftable td {
            border-top: thin solid;
            border-bottom: thin solid;
        }

        #brieftable td:first-child {
            border-left: thin solid;
        }

        #brieftable td:last-child {
            border-right: thin solid;
        }

    </style>
</head>
<body >
<div >
    <div class="page">

       <table width="100%">
           <thead>
               <th width="20%"><img style="float: right;" src="img/logo.jpg" width="110px" height="40px" alt="logo" /> <p style="font-size: 12px;">Kristl Star  Gh .Ltd</p><p style="font-size: 12px;">{{date('d-m-Y')}}</p></th>
               <th width="80%"><p style="font-size: 12px;">{{$product}} @ {{$depotname}}</p></b><p>{{$bdc}}</p></th>
               <th width="20%">
                   <p style="font-size: 12px;">Shore Tank :{{$constant->vcf}}</p>
                   <p style="font-size: 12px;">Dens @ 15C :{{$constant->density}}</p>
                   <p style="font-size: 12px;">Tem Deg C : {{$constant->temDeg}}</p>
               </th>
           </thead>
       </table>



        <div>
            <div>

                    <table id="datarecord" style="width: 100%;">
                        <thead>
                        <tr>
                            <th>Vehicle Number</th>
                            <th>Drivers Name</th>
                            <th>Loading Start</th>
                            <th>Load Stop</th>
                            <th>Dept. Time</th>

                            <th>Order Number</th>
                            <th>Weighbill No.</th>
                            <th>Customer</th>
                            <th>Nat Litres</th>
                            <th>Litres @ 15C</th>
                            <th>Metric Tons Air</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalObsLitres=0;
                        $totallitres=0;
                        $metrictonair=0;
                        ?>

                        @foreach($dailyrecord as $s)
                            <?php
                            $totalObsLitres = $s->obslitre + $totalObsLitres;
                            $totallitres = $s->litre + $totallitres;
                            $metrictonair = $s->tonmetricair + $metrictonair;

                            ?>

                                <tr>
                                    <td>{{$s->vehiclenumber}}</td>
                                    <td>{{$s->drivername}}</td>
                                    <td>{{$s->loadingstart}}</td>

                                    <td>{{$s->loadstart}}</td>
                                    <td>{{ $s->Depttime}}</td>

                                    <td>{{$s->orderno}}</td>
                                    <td>{{$s->waybillno}}</td>
                                    <td>{{ $s->customer}}</td>
                                    <td>{{number_format($s->obslitre)}}</td>
                                    <td>{{sprintf('%0.3f', $s->litre)}}</td>
                                    <td>{{sprintf('%0.3f ', $s->tonmetricair)}}</td>
                                </tr>
                        <tbody>



                        @endforeach


                        <tfoot>
                        <tr>
                            <td style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>


                        </tr>
                        <tr>
                            <td style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td  style="border: none;"></td>
                            <td><b>Sub Totals</b></td>
                            <td>{{number_format($totalObsLitres)}}</td>
                            <td>{{sprintf('%0.3f',$totallitres)}}</td>
                            <td>{{sprintf('%0.3f',$metrictonair)}}</td>


                        </tr>
                        </tfoot>

                    </table>



            </div>



        </div>
    </div>

</div>


</body>
</html>