<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Stocktape</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="asset/css/toastr.min.css" rel="stylesheet" />


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
@include('layout.admin.navbarToplayout')

<div class="main">

    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget ">

                        <div class="widget-header" style="background: white !important; ">

                            <h3 style="color:darkred !important;">Field Rep Records</h3>
                        </div> <!-- /widget-header -->
                    </div> <!-- /widget-content -->

                    {{--<div><center> <span>TempDeg:<strong> {{$constant->temDeg}}</strong></span>&nbsp; &nbsp;<span>Density: <strong>{{$constant->density}}</strong></span>&nbsp; &nbsp; <span>VCF: <strong>{{$constant->vcf}}</strong></span> </center></div>--}}
                    <table id="table" class="table table-hover display  pb-30" style = "font-size: 12px">
                        <thead style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Consignment Name</th>

                            <th>VehicleNumber</th>
                            <th>driver name</th>

                            <th>shore tank</th>
                            <th>Order Number</th>
                            <th>Way Bill Number</th>
                            <th>Customer</th>
                            <th>Obs Litre</th>
                            <th>Litre</th>
                            <th>VAC</th>
                            <th>Ton metric in air</th>
                            <th>Action </th>

                        </tr>
                        </thead>
                        <tfoot style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Consignment Name</th>
                            <th>VehicleNumber</th>
                            <th>driver name</th>
                            <th>shore tank</th>
                            <th>Order Number</th>
                            <th>Way Bill Number</th>
                            <th>Customer</th>
                            <th>Obs Litre</th>
                            <th>Litre</th>
                            <th>VAC</th>
                            <th>Ton metric in air</th>
                            <th>Action </th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        $i=1;
                        ?>
                        @foreach($dailyrecord as $d )
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$d->consignmentname}}</td>
                                <td>{{$d->vehiclenumber}}</td>
                                <td>{{$d->drivername}}</td>
                                <td>{{$d->shoretank}}</td>
                                <th>{{$d->orderno}}</th>
                                <td>{{$d->waybillno}}</td>
                                <td>{{$d->customer}}</td>
                                <td>{{$d->obslitre}}</td>
                                <td>{{$d->litre}}</td>
                                <td>{{ sprintf('%0.3f',$d->vac)}}</td>
                                <td>{{sprintf('%0.3f',$d->tonmetricair)}}</td>
                                <td>
                                    <div class="controls">
                                        <div class="btn-group">
                                            <a class="btn " style="background-color:white;color: darkred" href="#"><i class="icon-action icon-white"></i>Select</a>
                                            <a class="btn  dropdown-toggle" style="background-color:darkred;color: #fff" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-toggle="modal"  data-target="#editBdcModal" class="editbtn"  data-id="{{$d->id}}" data-vehiclenumber="{{$d->vehiclenumber}}" data-drivername="{{$d->drivername}}"
                                                       data-loadingstart="{{$d->loadingstart}}"
                                                       data-loadstart="{{$d->loadstart}}"
                                                       data-depttime="{{$d->Depttime}}"
                                                       data-shoretank="{{$d->shoretank}}"
                                                       data-orderno="{{$d->orderno}}"
                                                       data-waybillno="{{$d->waybillno}}"
                                                       data-customer="{{$d->customer}}"
                                                       data-obslitre="{{$d->obslitre}}"
                                                       data-consignmentname="{{$d->consignmentname}}"

                                                       data-tempdeg="{{$d->tempdeg}}"
                                                       data-density="{{$d->density}}"
                                                       data-vcf="{{$d->vcf}}"
                                                    ><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a class="deletebtn"  data-toggle="modal"  data-target="#deleteBdcModal" data-id="{{$d->id}}" ><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>	<!-- /controls -->
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> <!-- /widget -->

            </div> <!-- /span8 -->





            <div id="editBdcModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Daily Report</h4>
                        </div>
                        <form action="updatedailyapprove" method="post">
                            <input type="hidden" name="temdegedit" id="temdegedit" />
                            <input type="hidden" name="densityedit" id="densityedit" />
                            <input type="hidden" name="vcfedit" id="vcfedit" />
                            <div class="modal-body">
                                <input type="hidden" id="idedit" name="idedit" />
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Consignment NAme</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="consignmentnameedit" name="consignmentnameedit"  required disabled>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Vehicle Number</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="vehiclenumberedit" name="vehiclenumberedit"  required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Driver Name</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="drivernameedit" name="drivernameedit" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Loading Start</label>
                                    <div class="controls">
                                        <input type="time" class="span5" id="loadingstartedit" name="loadingstartedit" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Load Stop</label>
                                    <div class="controls">
                                        <input type="time" class="span5" id="loadstartedit" name="loadstartedit" required >
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Departure time</label>
                                    <div class="controls">
                                        <input type="time" class="span5" id="depttimeedit" name="depttimeedit" required >
                                    </div> <!-- /controls -->
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="firstname">Shore Tank</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="shoretankedit" name="shoretankedit" required>
                                    </div> <!-- /controls -->
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">Order  Number</label>

                                    <div class="controls">
                                        <input type="text" class="span5" id="ordernumberedit" name="ordernumberedit" required>
                                    </div>
                                </div> <!-- /control-group -->
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">Way Bill Number</label>

                                    <div class="controls">
                                        <input type="text" class="span5" id="waybillnumberedit" name="waybillnumberedit" required>
                                    </div>
                                </div> <!-- /control-group -->
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">customer</label>

                                    <div class="controls">

                                        <select id="customeredit" name="customeredit" required>
                                            @foreach(\Stocktape\Omc::all() as $s)
                                                <option value="{{$s->name}}">{{$s->name}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div> <!-- /control-group -->
                                <div class="control-group">
                                    <label class="control-label" for="radiobtns">OBs Litre</label>

                                    <div class="controls">
                                        <input type="number" class="span5" step="any" id="obslitreedit" name="obslitreedit" required>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Approve</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="deleteBdcModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete BDC</h4>
                        </div>
                        <form action="deletedailyapprove" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="iddelete" name="iddelete" />

                                <p>Are you sure u want to delete ?</p>
                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:#00ba8b;color: #fff" class="btn " >Delete</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /main-inner -->

</div> <!-- /main -->








<script src="asset/js/jquery-1.7.2.min.js"></script>

<script src="asset/js/bootstrap.js"></script>
<script src="asset/js/base.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="asset/js/toastr.min.js"></script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
    }
    @endif

</script>
<script>
    $(document).on('click','.editbtn',function() {
        //alert("ok");
        $('#idedit').val($(this).data('id'));
        $('#vehiclenumberedit').val($(this).data('vehiclenumber'));
        $('#consignmentnameedit').val($(this).data('consignmentname'));
        $('#drivernameedit').val($(this).data('drivername'));
        $('#shoretankedit').val($(this).data('shoretank'));
        $('#ordernumberedit').val($(this).data('orderno'));


        $('#loadingstartedit').val($(this).data('loadingstart'));
        $('#loadstartedit').val($(this).data('loadstart'));
        $('#depttimeedit').val($(this).data('depttime'));

        $('#waybillnumberedit').val($(this).data('waybillno'));
        $('#customeredit').val($(this).data('customer')).select();
        $('#obslitreedit').val($(this).data('obslitre'));


        $('#temdegedit').val($(this).data('tempdeg'));
        $('#densityedit').val($(this).data('density'));
        $('#vcfedit').val($(this).data('vcf'));


    });
    $(document).on('click','.deletebtn',function() {
        $('#iddelete').val($(this).data('id'));

    });
</script>
</body>

</html>
