<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Stocktape</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="asset/css/bootstrap.min.css" rel="stylesheet">
    <link href="asset/css/bootstrap-responsive.min.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="asset/css/font-awesome.css" rel="stylesheet">

    <link href="asset/css/style.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="asset/css/toastr.min.css" rel="stylesheet" />


    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>

<body>
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                        class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="dailyrecord">  Stocktape <img  src="img/logo.jpg" width="60px" height="60px" alt="logo" />  &nbsp;<h5><i class="icon-user"></i> Hello,{{Sentinel::getUser()->first_name}}</h5></a>
            <div class="nav-collapse">
                <ul class="nav pull-right">


                    @if(Sentinel::check())

                        <form action="{{route('logout')}}" method="post" id="logout-form">
                            {{csrf_field()}}
                            <a class="btn" style="color: darkred;background-color: white;" href="#" onclick="document.getElementById('logout-form').submit()" style="color: black"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>

                        </form>


                    @endif

                </ul>

            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /container -->
    </div>
    <!-- /navbar-inner -->
</div>

<!-- /navbar -->
<div class="subnavbar">
    <div class="subnavbar-inner">
        <div class="container">
            <ul class="mainnav">
                <li ><a href="omcfieldrep"><i class="icon-dashboard"></i><span>Omc</span> </a> </li>
                <li ><a href="dailyrecord"><i class="icon-list"></i><span>Daily Record</span> </a> </li>

            </ul>
        </div>
        <!-- /container -->
    </div>
    <!-- /subnavbar-inner -->
</div>
<!-- /subnavbar -->
<div class="main">


    <div class="main-inner">

        <div class="container">

            <div class="row">

                <div class="span12">

                    <div class="widget ">

                        <div class="widget-header" style="background: white !important; ">

                            <h3 style="color:darkred !important;">Omc</h3>
                        </div> <!-- /widget-header -->
                    </div> <!-- /widget-content -->
                    <button class="btn btn-lg" style="background-color:darkred;color: #fff" data-toggle="modal" data-target="#addbankModal">Add New</button><span>&nbsp;</span>


                    <table id="table" class="table table-hover display  pb-30" style = "font-size: 12px">
                        <thead style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th>Omc Name</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                        <tfoot style = "font-size: 12px">
                        <tr style = "font-size: 12px">
                            <th>S/N</th>
                            <th> Name</th>

                            <th>Action</th>
                        </tr>
                        </tfoot>
                        <?php
                        $i=1;
                        ?>
                        @foreach($omcs as $omc)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$omc->name}}</td>

                                <td>
                                    <div class="controls">
                                        <div class="btn-group">
                                            <a class="btn " style="background-color:white;color: darkred" href="#"><i class="icon-action icon-white"></i>Select</a>
                                            <a class="btn  dropdown-toggle" style="background-color:darkred;color: #fff" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a data-toggle="modal"  data-target="#editbankModal" class="editbtn" data-id="{{$omc->id}}" data-name="{{$omc->name}}"><i class="icon-pencil"></i> Edit</a></li>
                                                <li><a data-toggle="modal"  data-target="#deletebankModal" class="deletebtn" data-id="{{$omc->id}}" data-name="{{$omc->name}}"><i class="icon-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>	<!-- /controls -->
                                </td>

                            </tr>
                            @endforeach
                            </tbody>
                    </table>


                </div> <!-- /widget -->

            </div> <!-- /span8 -->



            <!-- Modal -->
            <div id="addbankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">New Omc</h4>
                        </div>
                        <form action="saveomc" method="post">
                            <div class="modal-body">

                                <div class="control-group">
                                    <label class="control-label" for="firstname">Omc Name</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="name" name="name"  required>
                                    </div> <!-- /controls -->
                                </div>



                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="editbankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit Omc</h4>
                        </div>
                        <form action="updateomc" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="bank_edit_id" name="idedit" />
                                <div class="control-group">
                                    <label class="control-label" for="firstname">Omc Name</label>
                                    <div class="controls">
                                        <input type="text" class="span5" id="name_edit" name="nameedit"  required>
                                    </div> <!-- /controls -->
                                </div>



                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Save Changes</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <!-- Modal -->
            <div id="deletebankModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete Omc</h4>
                        </div>
                        <form action="deleteomc" method="post">
                            <div class="modal-body">
                                <input type="hidden" id="bank_delete_id" name="iddelete" />

                                <p>Are you sure you want to delete <span id="nameOfbank"></span></p>
                            </div>
                            <div class="modal-footer form-actions">
                                {{csrf_field()}}
                                <button type="submit" style="background-color:darkred;color: #fff" class="btn " >Delete</button>

                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div> <!-- /row -->

    </div> <!-- /container -->

</div> <!-- /main-inner -->

</div> <!-- /main -->








<script src="asset/js/jquery-1.7.2.min.js"></script>

<script src="asset/js/bootstrap.js"></script>
<script src="asset/js/base.js"></script>
<script>
    $(document).ready(function() {
        $('#table').DataTable();
    } );
</script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="asset/js/toastr.min.js"></script>
<script src="asset/js/utility.js"></script>
<script>
            @if(Session::has('message'))
    var type="{{Session::get('alert-type','info')}}"
    switch(type){
        case 'success':
            toastr.success('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'error':
            toastr.error('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
        case 'info':
            toastr.info('{{Session::get('message')}}','Success',{timeOut: 3000});

            break;
    }
    @endif

</script>
<script>
    $(document).on('click','.editbtn',function() {

        $('#bank_edit_id').val($(this).data('id'));
        $('#name_edit').val($(this).data('name'));
//        $('#address_edit').val($(this).data('address'));
//        $('#phonenumber_edit').val($(this).data('phonenumber'));


    });
    $(document).on('click','.deletebtn',function() {
        $('#bank_delete_id').val($(this).data('id'));
        $('#nameOfbank').html($(this).data('name'));
    });
</script>
</body>

</html>
